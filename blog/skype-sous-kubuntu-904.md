Title: Skype sous Kubuntu 9.04  
Date: 2009-10-02 20:04  
Category: Communication  
Tags:  

Slug: skype-sous-kubuntu-904  
Status: published  

![skype\_logo](./public/2009/skype.png "skype_logo, oct. 2009")

Comme vous le savez [skype c'est mal](http://blog.philpep.org/main.py/post/Skype-:-un-logiciel-qui-vous-veut-du-bien). Mais quand vous trouvez un logiciel fiable qui gère la conversation webcam linux&lt;&gt;windows et bien vous l'utiliser.

En attendant l'arrivée proche du support de la webcam sous jabber/gtalk avec [Pidgin](http://www.pidgin.im/) sous windows, je détail ici l'installation de skype sous ubuntu 9.04.

La procédure se fait en 2 étapes ; ajout du dépôt [medibuntu](https://help.ubuntu.com/community/Medibuntu), installation de skype.

Ajout du dépôt medibuntu.
-------------------------

Suivre la démarche décrite sur [Ubuntu-fr](http://doc.ubuntu-fr.org/medibuntu)

Installation de skype
---------------------

`sudo apt-get install skype`

Et vous voici avec une installation toute fraiche de skype 2.0.

Et ça marche pas ....encore
---------------------------

Je rapporte ici les soucis que j'ai rencontré avant de pouvoir utiliser skype normalement.

### Problème de son

Allez faire un tour du coté de Options &gt; Son  
Faites joujou avec les options disponibles pour utiliser la bonne entrée, N'hésitez pas a faire des essais des pour vérifier que vous avec fais le bon choix.

### Son du micro faible

Mes interlocuteurs m'entendaient mal. J'ai trouvé l'origine du soucis qui est un simple réglage du volume micro trop bas.Sous Kubuntu, lancer le logiciel kmix et vérifiez que l'entrée "Internal Mic" est bien remontée.

Pour les amateurs du terminal ils peuvent faire la même chose avec

`alsamixer`

</p>

