Title: Conquérir le monde avec Linux  
Date: 2009-10-03 12:22  
Category: Jeux  
Tags:  

Slug: conquerir-le-monde-avec-linux  
Status: published  

![Risk](./public/2009/risk.png "Risk, oct. 2009")

[Risk](http://domination.sourceforge.net/) est un jeu de stratégie. On peut jouer seul contre l'ordinateur ou en réseau.

Déroulement de l'installation
-----------------------------

### Java

Vérifié que java est bien installé sur votre distribution.

### Télécharger l'installateur

Rendez vous sur le lien suivant <http://sourceforge.net/projects/risk/> pour télécharger **Risk\_install\_1.0.9.8.jar**

### Installation

ouvrez un terminal puis tapez

`java -jar /chemin/vers/Risk_install_1.0.9.8.jar`

pour ceux qui sont pas à l'aise avec le terminal vous pouvez simplement tapez "java -jar" puis glissez-déposez le fichier d'installation sur la fenêtre du terminal ;)

validez et procédez à l'installation

Plus qu'a jouer
---------------

Lors de l'installation, Risk crée un répertoire Risk dans le répertoire personnel, il ne faut pas y toucher. Pour lancer le jeu,

`Risk/run.sh`

Désinstallation
---------------

`java -jar  Risk/Uninstaller/uninstaller.jar`

</p>

