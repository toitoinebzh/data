Title: Mise en place d'une seedbox automatisée  
Date: 2018-10-12 16:41  
Category: Distribution  
Tags:  

Slug: mise-en-place-dune-seedbox-automatisee  
Status: published  

Une seedbox est un serveur dédié au partage de fichiers.
Je présente ici différents outils que j'ai mis en place afin de transformer un vieux pc en une seedbox automatisée.
L'objectif est de partager des iso de distributions gnu/linux par le protocole bittorrent et d'automatiser le tout (ajout automatique des dernières versions de distributions publiées).

Le matériel choisi est une simple tour trouvée sur leboncoin avec les caractéristiques suivantes :

  * Intel Core 2 Duo processor E8400 (2C/2T, 3.0 GHz, 6 MB, 1333 MHz)
  * 2 x 2 GB DDR2, 800 MHz, PC2-6400, DIMM
  * SATA II, 7200 rpm, 500 GB, 3.5-inch, S.M.A.R.T.
  * NVIDIA GeForce 9300GE, 256 MB
  * Carte mère μATX D2841


Cette mise en place se réalise en plusieurs étapes listées ci-dessous.

# Installation d'une distribution Gnu/Linux

Tout d'abord, il est nécessaire d'installer une distribution gnu/linux sur le pc.
Je ne détaille pas cette étape, les seules recommandations que je donne ici sont d'installer une version serveur (sans interface graphique type KDE, Gnome, ...) et de choisir une distribution avec un support sur le long terme (pour éviter les réinstallations tous les 6 mois).

La distribution utilisée ici est ubuntu 18.04 LTS, peut-être pas le meilleur choix mais largement suffisant pour se faire la main et comprendre comment fonctionne un serveur.

Dans le cadre de cet exemple, 

  * Ninja représente le serveur
  * Frankenstein est un client
  * le client et le serveur sont sur le même réseau local
  * antoine est le nom de l'utilisateur du serveur et du client.

Le disque dur du serveur a été partitionné de manière à séparer / de /home et une partition /document a également été crée.

```
antoine@Ninja:~$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465,8G  0 disk 
├─sda1   8:1    0  19,5G  0 part /
├─sda2   8:2    0  19,5G  0 part /home
├─sda3   8:3    0     2G  0 part [SWAP]
└─sda4   8:4    0 424,8G  0 part /document
sr0     11:0    1  1024M  0 rom  
antoine@Ninja:~$ 
```

Une fois l'installation terminée, il est nécessaire de s'assurer de la mise en place de l'accès ssh en installant le paquet openssh-server sur le serveur.

`sudo apt install openssh-server`

A noter que les dépendances de ce paquet installeront tous ce qui est nécessaire à la mise en place d'un accès sftp.

Le serveur peut maintenant être débranché de tous ces périphériques (écran, clavier, souris) seul l'alimentation et le cable éthernet sont nécessaires.

Pour accéder maintenant au serveur depuis n'importe quel client, il suffit de taper

`antoine@Frankenstein:~$ ssh antoine@Ninja`

On a alors accès au terminal du serveur. On peut par exemple éteindre le pc à distance avec un 

`antoine@Ninja:~$ sudo shutdown -h now`


A noter également que, par défaut, l'utilisateur antoine, n'aura pas les droits d'écriture/lecture sur /document, cela se règle simplement avec un 

`sudo chown -v $USER:$USER /document`


## Mise à jour automatique (optionnel)

Afin de réduire les étapes d'administration, il est possible de configurer le serveur de manière à se mettre à jour de manière automatique.

Pour plus d'infos, il est possible d'en savoir plus sur
[kanjian.fr](https://www.kanjian.fr/automatiser-mises-a-jour-de-serveur-debian.html)
et sur 
[ubuntu-fr.org](https://guide.ubuntu-fr.org/server/automatic-updates.html)
.

Tout d'abord, il faut installer le paquet unattended-upgrades

`sudo apt install unattended-upgrades`

Puis éventuellement, le configurer en éditant les fichiers 50unattended-upgrades et 20auto-upgrades.

Pour éditer le premier fichier, 
`antoine@Ninja:~$ sudo nano /etc/apt/apt.conf.d/50unattended-upgrades`

Ce fichier permet de choisir (lignes à commenter/décommenter) quels catégories de paquets seront mis à jour d'une part

```
Unattended-Upgrade::Allowed-Origins {
        "${distro_id}:${distro_codename}";
        "${distro_id}:${distro_codename}-security";
        "${distro_id}:${distro_codename}-updates";
//        "${distro_id}:${distro_codename}-proposed";
//        "${distro_id}:${distro_codename}-backports";
};
```

D'autre part, on peut également noter la présence de la ligne suivante dans le fichier qui permet de planifier la mise à jour au moment de l'extinction du pc (option intéressante sur un pc de bureau).


`Unattended-Upgrade::InstallOnShutdown "true";`

Le second fichier permet, quant à lui, de configurer la fréquence à laquelle les opérations de mise à jour seront réalisées (unité en jour).

`antoine@Ninja:~$ sudo nano /etc/apt/apt.conf.d/20auto-upgrades`


```
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
```

## Quelques outils pour contrôler l'état du pc (optionnel)

Uns fois que le pc tourne en continu, il peut être bon de s'y connecter de temps en temps pour voir comment les choses vont. Voilà quelques outils intéressants.

### Processus

Pour connaitre les processus en cours et voir si il y a emballement par exemple, rien de tel que htop

`htop`

### Sensors

Sensors est un outil qui permet de suivre la température et la vitesse des ventilateurs.

Pour l'installer,

```
sudo apt install lm-sensors
sudo sensors-detect
sudo service kmod restart # ou redémarrer
```

pour le lancer,

`sensors`

### Disque dur

Tout d'abord, il peut être intéressant de savoir si les disques sont pleins avec par exemple,

`df -h`

Pour l'état de santé des disques, smartmontools est le bon point d'entrée.
Il s'installe avec un  

`sudo apt install smartmontools`

et s'utilise avec 

`sudo smartctl -s on -a /dev/sda`

Il y a également [iotop](https://la-vache-libre.org/iotop-un-equivallent-de-top-dedie-aux-acces-disques/) qui est un outil qui permet de connaitre quelle application sollicite le plus votre disque dur 

`sudo iotop`

sans oublier ncdu qui est pas mal pour connaitre l'occupation du disque également

`ncdu`

## Trafic réseau


Dans le but de suivre le trafic réseau, je conseille également l'usage de [speedometer](https://la-vache-libre.org/speedometer-2-8-mesurez-facilement-le-trafic-reseau-de-vos-connexions/)

`speedometer -r enp3s0 -t enp3s0 -k 256`


![speedometer](./public/2018/octobre/speedometer.png)

# Vue globale

En dehors de ces outils dédiés à des tâches particulières, il y a également deux outils intéressants pour avoir une vue globale sur l'état du serveur.

Le premier est glances qui fonctionne est ligne de commande,

![glances](./public/2018/octobre/glances.png)

le second est phpsysinfo. Celui-ci s'installe via les commandes suivantes

```
sudo apt install phpsysinfo
cd /var/www/html
sudo ln -s /usr/share/phpsysinfo phpsysinfo
```

L'interface est ensuite accessible dans le navigateur via l'adresse ninja/phpsysinfo

![phpsysinfo](./public/2018/octobre/phpsysinfo.png)



# Mise en place de la seedbox

## Installation et configuration de transmission 

L'objectif ici est d'installer le logiciel bittoreent afin de transformer le serveur en une seedbox.

Cette partie s'inspire de plusieurs sources : [ici](https://www.guillaume-leduc.fr/la-seedbox-facile-sous-debian-avec-transmission.html), [là](https://angristan.fr/seedbox-installer-client-torrent-transmission-debian-ubuntu/) et [là aussi](http://voidandany.free.fr/index.php/installer-et-configurer-transmission-en-tant-que-demon/).

L'installation se réalise simplement par un 

`antoine@Ninja:~$ sudo apt install transmission-daemon ` 

La configuration demande plus d'efforts, il nécessite la modification du fichier settings.json.

`sudo nano /etc/transmission-daemon/settings.json`

On note principalement la présence des lignes suivantes (à modifier si nécessaire).

  * download-dir pour définir où seront stockés les téléchargements.
  * incomplete-dir pour ceux en cours (mettre incomplete-dir-enabled à true pour mettre les fichiers incomplets dans incomplete-dir.
  * watch-dir est un dossier surveillé par transmission, tous .torrent ajoutés dans ce dossier est automatiquement ajouté dans le logiciel (on reparlera de cette fonctionnalité un peu plus loin).

```
"download-dir": "/document/torrent/complete",
"incomplete-dir": "/document/torrent/incomplete",
"incomplete-dir-enabled": true,
"watch-dir": "/document/torrent/watch",
"watch-dir-enabled": true
```

  * les options rpc-* permettent de mettre en place une interface web qui permettra de suivre à distance l'avancement des téléchargements. Il est possible définir les ip qui peuvent avoir accès à cette interface avec les options whitelist, username et password pour le login d'accès à l'interface


```
"rpc-authentication-required": true,
"rpc-bind-address": "0.0.0.0",
"rpc-enabled": true,
"rpc-host-whitelist": "",
"rpc-host-whitelist-enabled": true,
"rpc-password": "motdepasse",
"rpc-port": 9091,
"rpc-url": "/transmission/",
"rpc-username": "antoine",
"rpc-whitelist": "127.0.0.1,192.168.1.*",
"rpc-whitelist-enabled": true,
```

L'interface web est alors accessible en tapant ninja:9091 dans la barre d'adresse du navigateur.

![transmission_web](./public/2018/octobre/transmission_web.png)


Au final, le fichier settings.json ressemble à ceci 

```
antoine@Ninja:/document/torrent$ sudo cat  /etc/transmission-daemon/settings.json 
{
    "alt-speed-down": 50,
    "alt-speed-enabled": false,
    "alt-speed-time-begin": 540,
    "alt-speed-time-day": 127,
    "alt-speed-time-enabled": false,
    "alt-speed-time-end": 1020,
    "alt-speed-up": 50,
    "bind-address-ipv4": "0.0.0.0",
    "bind-address-ipv6": "::",
    "blocklist-enabled": false,
    "blocklist-url": "http://www.example.com/blocklist",
    "cache-size-mb": 4,
    "dht-enabled": true,
    "download-dir": "/document/torrent/complete",
    "download-limit": 100,
    "download-limit-enabled": 0,
    "download-queue-enabled": true,
    "download-queue-size": 5,
    "encryption": 1,
    "idle-seeding-limit": 30,
    "idle-seeding-limit-enabled": false,
    "incomplete-dir": "/document/torrent/incomplete",
    "incomplete-dir-enabled": true,
    "lpd-enabled": false,
    "max-peers-global": 200,
    "message-level": 1,
    "peer-congestion-algorithm": "",
    "peer-id-ttl-hours": 6,
    "peer-limit-global": 200,
    "peer-limit-per-torrent": 50,
    "peer-port": 51413,
    "peer-port-random-high": 65535,
    "peer-port-random-low": 49152,
    "peer-port-random-on-start": false,
    "peer-socket-tos": "default",
    "pex-enabled": true,
    "port-forwarding-enabled": true,
    "preallocation": 1,
    "prefetch-enabled": true,
    "queue-stalled-enabled": true,
    "queue-stalled-minutes": 30,
    "ratio-limit": 2,
    "ratio-limit-enabled": false,
    "rename-partial-files": true,
    "rpc-authentication-required": true,
    "rpc-bind-address": "0.0.0.0",
    "rpc-enabled": true,
    "rpc-host-whitelist": "",
    "rpc-host-whitelist-enabled": true,
    "rpc-password": "motdepasse",
    "rpc-port": 9091,
    "rpc-url": "/transmission/",
    "rpc-username": "antoine",
    "rpc-whitelist": "127.0.0.1,192.168.1.*",
    "rpc-whitelist-enabled": true,
    "scrape-paused-torrents-enabled": true,
    "script-torrent-done-enabled": false,
    "script-torrent-done-filename": "",
    "seed-queue-enabled": false,
    "seed-queue-size": 10,
    "speed-limit-down": 100,
    "speed-limit-down-enabled": false,
    "speed-limit-up": 100,
    "speed-limit-up-enabled": false,
    "start-added-torrents": true,
    "trash-original-torrent-files": false,
    "umask": 18,
    "upload-limit": 100,
    "upload-limit-enabled": 0,
    "upload-slots-per-torrent": 14,
    "utp-enabled": true,
    "watch-dir": "/document/torrent/watch",
    "watch-dir-enabled": true
}
```


Une fois le fichier modifié et enregistré, il faut relancer transmission pour prendre en compte les modifications 

`service transmission-daemon reload`

Comme on peut le remarquer un peu plus haut, des chemins vers "download-dir", "incomplete-dir" et "watch-dir" ont été définis.
Ces dossiers n'existent pas, il faut donc les créer à l'aide de la fonction mkdir.

La structure suivante a été mise en place dans le dossier /document

```
torrent/
├── complete
├── incomplete
└── watch
```

Cependant, il faut faire attention, ces dossiers ont été créés en tant que antoine(Ninja) alors que transmission-daemon est par défaut lancé avec un "utilisateur" appelé debian-transmission. Il risque donc d'avoir des conflits sur les permissions sur ces dossiers.

Il faut donc modifier les droits sur les dossiers 

```
antoine@Ninja:~$ sudo chown antoine:debian-transmission torrent/*
antoine@Ninja:~$ ls -la /document/torrent/
total 36
drwxrwxr-x 5 antoine antoine              4096 sept. 27 18:57 .
drwxr-xr-x 4 antoine antoine              4096 sept. 23 19:15 ..
drwxrwxr-x 5 antoine debian-transmission 12288 sept. 27 18:53 complete
drwxrwxr-x 3 antoine debian-transmission  4096 sept. 27 19:19 incomplete
drwxrwxr-x 2 antoine debian-transmission 12288 sept. 27 19:20 watch
```

à partir de maintenant, la seedbox est complétement fonctionnelle, pour lancer des torrents, plusieurs méthodes existent

  * lancement de l'interface web via ninja:9091 sur le pc client, il est possible d'ajouter un torrent via cette interface
  * ajout d'un torrent dans le "watch-dir" via par exemple un wget ou en utilisant par exemple filezilla (explication plus loin)
  * ajout via transmission-remote-gtk ou transmission-remote-cli, ces programmes permettent d'accéder à la seedbox depuis le pc client

## Outils transmission-remote-*  (optionnel)

transmission-remote-gtk et transmission-remote-cli sont deux programmes qui permettent de suivre l'état de la seedbox depuis un pc client. Le premier est en interface graphique alors que le second est en ligne de commande.

Pour les installer 

`sudo apt install transmission-remote-gtk transmission-remote-cli`

Pour utiliser transmission-remote-cli

`transmission-remote-cli -c antoine:motdepasse@Ninja:9091`

![transmission_cli](./public/2018/octobre/transmission_cli.png)


transmission-remote-gtk est assez simple d'utilisation et demandera le nom du serveur, du port et de l'utilisateur pour la configuration.

![transmission_remote](./public/2018/octobre/transmission_remote.png)


## Accès via Filezilla  (optionnel)

Pour pouvoir récupérer les fichiers téléchargés, il est plus agréable de passer par l'interface graphique que par la ligne de commande. Sur le pc client, il est par exemple possible d'utiliser un outil tel que Filezilla pour accéder aux fichiers du serveur.

![Configuration de Filezilla](./public/2018/octobre/config_filezilla.png)


# Seedbox automatisée  (optionnel)

L'idée d'une seedbox automatisée consiste à faire en sorte 

  * de surveiller l'apparition de nouveaux .torrent sur des sites internet/flux rss
  * les télécharger et les partager automatiquement pour favoriser leurs diffusions

L'idée m'est venue un peu par hasard en consultant [Peterp's Blog](https://ptp1.wordpress.com/2015/03/03/auto-linux-iso-torrent-seeder/). Il est à noter que certains logiciels bittorrent permettent de suivre des flux rss pour ajouter automatiquement des fichiers torrent, ce n'est pas le cas par défaut de transmission, voilà comment je m'y suis pris.


## Distrowatch

![dwbanner](./public/2018/octobre/dwbanner.png)


Tout d'abord, j'ai voulu mettre en place cette méthodologie en utilisant le flux rss de distrowatch qui diffusent régulièrement les .torrent des nouvelles distributions Gnu/Linux.

J'ai donc créé un script python qui lit ce flux rss, récupère les .torrent et les place dans le "watch-dir". Les torrents sont alors automatiquement ajoutés par transmission. Pour les personnes intéressées, ce script est disponible sur [framagit](https://framagit.org/toitoinebzh/distrowatchscraper).


Sur le serveur, on télécharge donc tout d'abord le script et on installe les paquets pour le faire tourner

```
sudo apt install python3-feedparser git
git clone https://framagit.org/toitoinebzh/distrowatchscraper.git
```
on modifie le fichier afin de définir le chemin vers "watch-dir" (variable TORRENT_LOCATION = "/document/torrent/watch/")

`nano distrowatchscraper/distrowatch_torrent_rss.py # modif de TORRENT_LOCATION`


Il suffit alors de lancer le script pour récupérer les derniers .torrent.

```
antoine@Ninja:~$ python3 distrowatchscraper/distrowatch_torrent_rss.py 
This program download every torrent on distrowatch.com rss feed
Downloading :  pfSense-CE-2.4.4-RELEASE-amd64.iso.gz.torrent
Downloading :  smartos-20180927T004151Z.iso.torrent
Already in the directory :  quirky-beaver64-8.7.1.iso.torrent
Already in the directory :  thinstation-5.6.0-Installer-0919.iso.torrent
Already in the directory :  osgeo-live-12.0-amd64.iso.torrent
Already in the directory :  linuxfx-ctos-lts-9.4-x64.iso.torrent
Already in the directory :  omarine-4.1-dvd.iso.torrent
Already in the directory :  systemrescuecd-x86-5.3.0.iso.torrent
Already in the directory :  nitrux_release_1.0.15-1.iso.torrent
Already in the directory :  tails-amd64-3.9.torrent
Already in the directory :  KaOS-2018.08-x86_64.iso.torrent
Already in the directory :  archman-deepin-1809_x86_64.iso.torrent
Already in the directory :  swagarch-1809_x86_64.iso.torrent
Already in the directory :  manjaro-kde-17.1.12-stable-x86_64.torrent
Already in the directory :  neon-useredition-20180830-1023-amd64.iso.torrent
Already in the directory :  smartos-20180830T001556Z.iso.torrent
Already in the directory :  mint-3-lmde-201808-cinnamon-64bit.iso.torrent
Already in the directory :  NetBSD-7.2-amd64.iso.torrent
Already in the directory :  backbox-5.2-amd64.iso.torrent
Already in the directory :  bluestar-linux-4.18.4-2018.08.24-x86_64.iso.torrent
Already in the directory :  Voyager-GS-18.04.1-amd64.iso.torrent
Already in the directory :  nst-28-10439.x86_64.iso.torrent
Already in the directory :  omarine-4.0-dvd.iso.torrent
Already in the directory :  gparted-live-0.32.0-1-amd64.iso.torrent
Already in the directory :  bodhi-5.0.0-64.iso.torrent
antoine@Ninja:~$ 
```


Evidemment, il est rébarbatif de lancer cette commande à chaque nouvelle sortie d'une distribution, on met donc en place une tache cron pour lancer ce script de manière régulière. ([Source d'inspiration](https://math-linux.com/linux-2/commande-du-jour/article/programmation-des-actions-taches-regulieres-crontab))

Tout d'abord on lance crontab

`antoine@Ninja:~$ crontab -e # execution sans sudo pour execution du script par antoine`

puis on ajoute la ligne suivante dans le fichier

`@daily python3 /home/antoine/distrowatchscraper/distrowatch_torrent_rss.py`

Cela va lancer le script quotidiennement. On a donc une seedbox complétement autonome.

## Autres scripts

à noter que j'ai créé d'autres scripts similaires pour récupérer les .torrent de [freetorrent.fr](http://freetorrent.fr/) et [FDN.fr](https://www.fdn.fr/). Les scripts sont [ici](https://framagit.org/users/toitoinebzh/projects).

# Ouverture des ports TCP/UDP (si nécessaire)

Dans le cas de difficultés de téléchargement, un moyen d'améliorer les choses peut être de configurer sa box de manière à ouvrir les ports udp/tcp utilisés par transmission (ici le port 51413).

![livebox](./public/2018/octobre/livebox.png)
