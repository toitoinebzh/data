Title: HP Pavilion t559.fr  
Date: 2011-07-30 15:20  
Category: Matériel  
Tags:  

Slug: hp-pavilion-t559fr  
Status: published  

![HP\_Pavilion\_t559.fr.jpg](./public/2011/HP_Pavilion_t559.fr.jpg "HP_Pavilion_t559.fr.jpg, juil. 2011")

Voici un pc que j'ai récemment fait migrer sous Ubuntu 10.10.

C'est un pc récupéré qui date un peu mais qui est idéal pour un usage bureautique/internet.

Niveau matériel c'est un P4 à 3 GHz avec 512 Mo de ram et une carte graphique Nvidia Geforce FX 5500.Il est équipé d'un lecteur et aussi d'un graveur de DVD.

En façade on peux noter la présence également d'un lecteur de disquette et d'un port firewire, 3 ports USB 2.0 et d'un lecteur 8 en 1 (SD, MMC, ....). Il y a aussi des prises pour l'audio, le micro.

Voilà ce que retourne un lspci pour plus de détails

    00:00.0 Host bridge: Intel Corporation 82865G/PE/P DRAM Controller/Host-Hub Interface (rev 02)  
    00:01.0 PCI bridge: Intel Corporation 82865G/PE/P PCI to AGP Controller (rev 02)  
    00:06.0 System peripheral: Intel Corporation 82865G/PE/P Processor to I/O Memory Interface (rev 02)  
    00:1d.0 USB Controller: Intel Corporation 82801EB/ER (ICH5/ICH5R) USB UHCI Controller #1 (rev 02)  
    00:1d.1 USB Controller: Intel Corporation 82801EB/ER (ICH5/ICH5R) USB UHCI Controller #2 (rev 02)  
    00:1d.2 USB Controller: Intel Corporation 82801EB/ER (ICH5/ICH5R) USB UHCI Controller #3 (rev 02)  
    00:1d.3 USB Controller: Intel Corporation 82801EB/ER (ICH5/ICH5R) USB UHCI Controller #4 (rev 02)  
    00:1d.7 USB Controller: Intel Corporation 82801EB/ER (ICH5/ICH5R) USB2 EHCI Controller (rev 02)  
    00:1e.0 PCI bridge: Intel Corporation 82801 PCI Bridge (rev c2)  
    00:1f.0 ISA bridge: Intel Corporation 82801EB/ER (ICH5/ICH5R) LPC Interface Bridge (rev 02)  
    00:1f.1 IDE interface: Intel Corporation 82801EB/ER (ICH5/ICH5R) IDE Controller (rev 02)  
    00:1f.2 IDE interface: Intel Corporation 82801EB (ICH5) SATA Controller (rev 02)  
    00:1f.3 SMBus: Intel Corporation 82801EB/ER (ICH5/ICH5R) SMBus Controller (rev 02)  
    00:1f.5 Multimedia audio controller: Intel Corporation 82801EB/ER (ICH5/ICH5R) AC'97 Audio Controller (rev 02)  
    01:00.0 VGA compatible controller: nVidia Corporation NV34 [GeForce FX 5500] (rev a1)  
    02:0a.0 Communication controller: Conexant Systems, Inc. HSF 56k HSFi Modem (rev 01)  
    02:0e.0 FireWire (IEEE 1394): Texas Instruments TSB43AB22/A IEEE-1394a-2000 Controller (PHY/Link)  
    02:0f.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL-8139/8139C/8139C+ (rev 10)
    
L'ensemble du matériel fonctionne correctement.Bien que jusqu'ici je n'ai pas encore eu l'occasion de tester le lecteur 8 en 1, le modem 56k et les ports firewire ainsi que le lecteur de disquette.

### Quelques infos supplémentaires :

[dmidecode](./public/2011/HP_Pavilion_t559.fr_dmidecode.txt)

[Documentation](./public/2011/HP_Pavilion_t559.fr.pdf)

[Documentation](./public/2011/HP_Pavilion_t559.fr_2.pdf)

</p>

