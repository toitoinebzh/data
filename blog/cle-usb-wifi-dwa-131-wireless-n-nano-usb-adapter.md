Title: Clé USB Wifi DWA-131 Wireless-N Nano USB Adapter  
Date: 2011-11-05 17:10  
Category: Matériel  
Tags:  

Slug: cle-usb-wifi-dwa-131-wireless-n-nano-usb-adapter  
Status: published  

![DWA-131.jpeg](./public/2011/DWA-131.jpeg "DWA-131.jpeg, nov. 2011")

Voilà une petite clé qui fonctionne directement sous Gnu/Linux (testé sous Xubuntu 11.04 et 11.10)

Quelques infos sur le lsusb

`Bus 003 Device 002: ID 07d1:3303 D-Link System DWA-131 802.11n Wireless N Nano Adapter(rev.A1) [Realtek RTL8192SU]`

</p>

