Title: Mon installation perso de Xubuntu 11.10  
Date: 2011-11-05 16:44  
Category: Distribution  
Tags:  

Slug: mon-installation-perso-de-xubuntu-1110  
Status: published  

Un petit récapitulatif de mon installation de Xubuntu 11.10 sur [mon pc](./nouveau-pc-le-tout-sous-gnulinux.md).

Je commence d'abord par faire une mise à jour.

`sudo apt-get update`  
`sudo apt-get dist-upgrade`

Ensuite pour les codecs, flash et java ...

`sudo apt-get install xubuntu-restricted-extras`

puis quelques logiciels

-   homebank : pour ma comptabilité perso
-   gcstar : pour gérer ma bibliothèque
-   banshee : pour écouter de la musique et surtout récupérer des podcasts audios et vidéos
-   liferea ; pour les flux rss
-   gajim : pour mon compte jabber
-   geany : utile pour coder ou lire des fichiers
-   vlc : parce qu'il lit toutes les musiques et vidéos
-   cheese : pour m'amuser avec ma webcam
-   gthumb : qui a plus de fonctionnalités que ristretto pour voir des images ou les éditer
-   filezilla : pour le ftp

`sudo apt-get install homebank gcstar banshee liferea gajim geany vlc cheese gthumb filezilla`

pour lire certaines vidéos sur le net

`sudo apt-get install gecko-mediaplayer`

enfin pour garder un œil sur la température de mon matériel

`sudo apt-get install lm-sensors fancontrol sensord read-edid i2c-tools`  
`sudo sensors-detect`

</p>

