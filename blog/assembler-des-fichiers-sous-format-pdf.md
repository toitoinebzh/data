Title: Assembler des fichiers sous format PDF  
Date: 2011-12-03 11:34  
Category: Bureautique  
Tags:  

Slug: assembler-des-fichiers-sous-format-pdf  
Status: published  

Petites astuces pour assembler des fichiers sous format PDF en ligne de commande

Une première solution qui utilise gs

`gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=sortie.pdf fichier1.pdf fichier2.pdf fichier3.pdf`

On peut aussi utiliser convert (ImageMagick)

`convert fichier1.jpg fichier2.jpg sortie.pdf`

</p>

