Title: Enregistrer et regarder la TV par internet  
Date: 2010-03-19 18:38  
Category: Multimedia  
Tags:  

Slug: enregistrer-et-regarder-la-tv-par-internet  
Status: published  

Enregistrer la TV
-----------------

Pour ceux qui regardent la TV par internet avec le service SFR, voilà un moyen d'enregistrer les vidéos.

En ligne de commande, tout se fait par mplayer

    mplayer -dumpstream -dumpfile arte`date +%d-%m-%Y_%H:%M`.mpeg -nocache http://86.64.159.229:8080/pctv_arte

Trouver la bonne chaine
-----------------------

Pour récupérer l'adresse nécessaire à mplayer (une adresse par chaine), il suffit de jeter un œil dans ce [fichier](http://tv.sfr.fr/tv-pc/televisionsurpc.m3u).

</p>

