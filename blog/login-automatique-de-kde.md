Title: Login automatique de KDE  
Date: 2009-10-08 15:26  
Category: Distribution  
Tags:  

Slug: login-automatique-de-kde  
Status: published  

![kde4\_logo](./public/2009/kde4_logo.png "kde4_logo, oct. 2009")

Pour ceux qui cherchent comment pour que la session kde s'ouvre automatiquement sans taper de mot de passe, voila la démarche à suivre

-   K &gt; Poste de travail &gt; Configuration du système &gt; onglet "Avancé"

Ensuite lancez le gestionnaire de connexion, le mot de passe sera demandé.

-   Gestionnaire de connexion &gt; onglet "Commodités"

![login\_auto\_kde](./public/2009/login_auto_kde.png "login_auto_kde, oct. 2009")

-   enfin cochez "activer la connexion automatique" puis choisissez l'utilisateur à connecter par défaut

</p>

