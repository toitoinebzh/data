Title: de Autocad à Qcad : conversion DWG > DXF  
Date: 2009-10-19 19:32  
Category: Mécanique  
Tags:  

Slug: de-autocad-a-qcad-conversion-dwg-dxf  
Status: published  

![qcad](./public/2009/qcad.png "qcad, oct. 2009")

Dans une démarche de migration de Autocad vers une application libre telle que [Qcad](http://www.qcad.org/), on peut se heurter à un problème de format de fichiers.

En effet, si l'on souhaite ouvrir ces fichiers**.DWG** créer sous Autocad, on se rend compte que ce format n'est pas supporté sous Qcad au contraire du format **.DXF**.

Ce qui présente alors la nécessité de convertir ces fichiers en .DXF.

Conversion DWG&lt;&gt;DXF
-------------------------

En allant sur le net, la première solution qui se présente est d'utiliser [ce shareware](http://www.adx-online.com/prog/DWG_DXF%20Converter.html) :( .

Pour l'utiliser, c'est très simple.

Décompresser le fichier **DWG-DXFConverter-lx.zip** télécharger [ici](http://www.adx-online.com/prog/DWG_DXF%20Converter.html).

Vous récupérez alors deux fichiers. Cliquez alors sur le fichier **DWG-DXF Converter**.

L'interface est simple d'utilisation, il faut juste faire attention au format de sorti qui existe en plusieurs versions.

Résultats
---------

Les résultats que j'ai obtenus sont vraiment bon. J'ai même été étonné d'une aussi bonne conversion sur les quelques fichiers que j'ai testés.

Mis à part des cotations qui ne sont pas passées; les hachures, couleurs et textes sont bien converties dans les fichiers .DXF.

Sources
-------

[On en parle sur le forum Ubuntu](http://forum.ubuntu-fr.org/viewtopic.php?id=200120)

[Lien vers le convertisseur DWG&lt;&gt;DXF](http://www.adx-online.com/prog/DWG_DXF%20Converter.html)

</p>

