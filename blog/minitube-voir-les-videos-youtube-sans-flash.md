Title: Minitube - voir les vidéos youtube sans Flash  
Date: 2009-11-12 20:03  
Category: Multimedia  
Tags:  

Slug: minitube-voir-les-videos-youtube-sans-flash  
Status: published  

![minitube](./public/2009/minitube.jpg "minitube, déc. 2009")

Minitube est un logiciel sous licence GPL qui permet de lire les vidéos disponibles sur youtube

-   sans avoir besoin d'utiliser un navigateur internet et surtout
-   **sans avoir besoin d'installer le plugin propriétaire flash.**

Installation
------------

Pour les utilisateurs d'Ubuntu, Minitube est présent dans le [dépôt getdeb](./ajout-des-depots-getdeb-sous-ubuntu-910) qu'il suffit d"ajouter.

Il est ensuite possible d'installer minitube depuis synaptic ou dans un terminal.

`sudo apt-get install minitube`

Les sources et paquets du logiciel pour les autres distributions sont accessibles à partir du site officiel.

Utilisation
-----------

Le logiciel est simple à utiliser, une simple recherche permet d'avoir accès à un grand nombre de vidéos facile à choisir. L'ensemble est assez ergonomique.

Le logiciel souffre de quels bugs et ne fonctionne pas encore très bien même si de nouvelles versions apparaissent fréquemment et laissent espérer des améliorations à court terme.

Globalement la lecture est plus lente qu'avec un navigateur et flash mais ça reste acceptable.

Pour les utilisateurs de Totem, sachez qu'il existe un greffon qui permet d'avoir cette même fonctionnalité.

Liens
-----

[Minitube](http://flavio.tordini.org/minitube)

[Totem et greffon youtube](http://doc.ubuntu-fr.org/totem)

</p>

