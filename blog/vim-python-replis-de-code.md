Title: Vim, Python & replis de code  
Date: 2009-10-13 19:14  
Category: Programmation  
Tags:  

Slug: vim-python-replis-de-code  
Status: published  

![vim\_animation](./public/2009/vim_animation.gif "vim_animation, oct. 2009")

Je viens de commencer à apprendre à coder en Python.

Après des débuts sous [Geany](http://www.geany.org/), me voilà sous [Vim](http://www.vim.org/).

Je débute tout juste et présente de petites astuces que j'ai découvert.

Vimrc
-----

Le fichier vimrc est le fichier de configuration de vim, par ce fichier il est possible de personnaliser de manière très poussée l'utilisation du logiciel.

voila ce que me renvoie

`cat ~/.vimrc`

    if has("syntax") "active la coloration syntaxique  
    syntax on  
    endif  
    set number "numérotation des lignes  
    set mouse =a "utilsation dela souris  
    autocmd BufRead *.py nmap <F5> :!python %<CR> "la touche F5 lance le fichier

Autocomplétion
--------------

Le détail qui m'a donné envie de passé de Geany à Vim était l'autocomplétion.

Il faut reconnaitre que l'autocomplétion est plus performante sous Vim.

Le raccourci à connaitre lorsqu'on est sous Vim est `Ctrl + p` ou `Ctrl + n` pour autocompléter rapidement.

Replis de code
--------------

Une fois sous Vim une des fonctionnalités qui me manquait était le replis de code (cf. animation au début du texte).

Je voulais que ces replis se fasse automatiquement en fonction du langage de programmation.

Après avoir fouiller la documentation de Vim la fonction magique à utiliser se révèle être

    set foldmethod=syntax

et là, c'est le drame :( , en effet cela marche bien pour des fichiers écrit en C mais pour Python rien, ça ne fonctionne pas.

Après un petit tour sur le salon jabber de vim-fr, je trouve une solution de dépannage.

Cette solution consiste à utiliser un plugin qui s'installe assez facilement.

Télécharger [ce plugin](http://www.vim.org/scripts/script.php?script_id=2527) puis coller le dans ~/.vim/plugin/ (créer les dossiers si ils n'existent pas ;) )

### Raccourci à connaitre :

z + o pour ouvrir un repli

z + c pour fermer un repli

Liens
-----

[Le plugin de repli python](http://www.vim.org/scripts/script.php?script_id=2527)

[Vim-fr](http://vim-fr.org/)

[Geany](http://www.geany.org/)

</p>

