Title: Thunderbird et Kde sont dans un bateau...  
Date: 2009-10-03 13:46  
Category: Communication  
Slug: thunderbird-et-kde-sont-dans-un-bateau  
Status: published  

.... lequel tombe à l'eau.

![thunderbird_logo](./public/2009/thunderbird.png)
![kde4_logo](./public/2009/kde4_logo.png)


## Description du problème

Un retour pour un problème que je rencontre pour la deuxième fois avec Kde.

Le problème ici est que je ne pouvais pas cliquer sur les liens internets présents dans mes mails de thunderbird.
J'utilise rarement kde mais j'ai rencontré ce soucis sous Kubuntu 9.04 avec Kde4  et il y a un peu plus longtemps sous Debian avec Kde3.

## Solution

Pour ceux confrontés aux mêmes problèmes voila la démarche à suivre.

    kate .mozilla-thunderbird/XXXXX.default/prefs.js

Remplacé XXXXX par le dossier qui concerne votre profil.
Attention, dans certaines distributions le dossier .mozilla-thunderbird s'appelle .thunderbird

Ajoutez ces deux lignes et enregistrez

    pref("network.protocol-handler.app.http","firefox");
    pref("network.protocol-handler.app.https","firefox");

Vous pouvez changer "firefox" par le nom de votre navigateur favoris ;) .

Thunderbird peut être lancé, les liens marcheront.