Title: Wolfenstein: Enemy Territory  sous Ubuntu 9.04  
Date: 2009-10-02 20:53  
Category: Jeux  
Tags:  

Slug: wolfenstein-enemy-territory-sous-ubuntu-904  
Status: published  

![Enemy\_Territory.jpg](./public/2009/Enemy_Territory.jpg "Enemy_Territory.jpg, oct. 2009")

Je viens récemment d'installer Wolfenstein: Enemy Territory sur ma Kubuntu 9.04. Je fais un retour sur la méthode d'installation qui s'inspire de la [documentation de ubuntu-fr](http://doc.ubuntu-fr.org/enemy_territory).

J'ai rencontré des problèmes pour avoir un jeu qui fonctionne bien, je les détaillerai à la fin.

Installation
------------

Une fois encore, tout se fait en ligne de commande (rien de tel pour faire peur aux débutants :( )

`wget -c http://ftp.games.skynet.be/pub/wolfenstein/et-linux-2.60.x86.run`

`sudo bash et-linux-2.60.x86.run`

Acceptez la licence et le chemin d'installation qu'il propose. Terminez l'installation.

Vous voila donc avec un jeu fraichement installé que vous pouvez lancer depuis le terminal avec

`et`

Mise à jour
-----------

Pour ceux qui souhaiterais faire la mise à jour voilà la procédure à suivre

    wget -c http://ftp.games.skynet.be/pub/wolfenstein/et-linux-2.60-update.x86.run
    sudo sh et-linux-2.60-update.x86.run
    wget -c http://www.esreality.com/files/misc/2006/38672-ET-2.60b-linux.zip
    sudo mv /usr/local/games/enemy-territory/et.x86 /usr/local/games/enemy-territory/eta.x86
    sudo mv /usr/local/games/enemy-territory/etded.x86 /usr/local/games/enemy-territory/etdeda.x86
    unzip 38672-ET-2.60b-linux.zip
    sudo cp Enemy\ Territory\ 2.60b/linux/et*.x86 /usr/local/games/enemy-territory/
    rm -f 38672-ET-2.60b-linux.zip
    rm -rf Enemy\ Territory\ 2.60b/
    sudo chown -R utilisateur\: ~/.etwolf   # changer utilisateur par votre nom d'utilisateur

Qui a dit que c'était simple linux ? :(

Maintenant vous êtes prêt à jouer avec la version la plus récente, à moins qu'il y ai un problème ...

Problème
--------

### Mauvais Clavier

Par défaut le jeu fonctionne comme si on avait un clavier qwerty. Je suis aller faire un tour dans les préférences et le tour est joué :) .

### Pas de son

Résolu en téléchargeant un fichier qui se charge de lancer le jeu.

     wget -q -O - http://nullkey.ath.cx/~stuff/et-sdl-sound/et-sdl-sound.gz | gzip -d > et-sdl-sound && chmod a+x et-sdl-sound

Il faut alors lancer le jeu de cette manière

`./et-sdl-sound`

### Violation game integrity \#20004

Lorsque vous tenter de vous connecter à un serveur, ce message d'erreur apparait parce que la version de punkbuster n'est pas à jour.

C'est le soucis que j'ai mis le plus de temps à résoudre et j'ai du pas mal bidouiller avant d'y arriver.

Télécharger [ce fichier](http://websec.evenbalance.com/downloads/linux/pbsetup.run) et placer le dans le dossier \~/.etwolf/pb. J'ai télécharger la version sans GUI.

    cd .etwolf/pb/
    chmod +x pbsetup.run 
    sudo upx -d pbsetup.run 
    ./pbsetup.run --add-game=et 
    ./pbsetup.run -u 
    ./pbsetup.run

</p>

