Title: Apt-get : installation sans paquets recommandés  
Date: 2010-06-06 12:11  
Category: Dépôt  
Tags:  

Slug: apt-get-installation-sans-paquets-recommandes  
Status: published  

![debian](./public/2010/debian.jpg "debian, nov. 2009")

Installation sans paquets recommandés avec apt
----------------------------------------------

Apt est l'outil de gestion de paquets que l'on retrouve sur les distributions basées sur Debian.

Lors de l'installation d'une application, cet outil gère automatiquement les dépendances c'est-à-dire qu'il installe automatiquement les paquets nécessaires au bon fonctionnement de l'application.

Il a aussi la fonctionnalité de pouvoir installer, en plus des dépendances, des paquets dits "recommandés" ou "suggérés".

Ces paquets recommandés, même si il ne sont pas nécessaire, sont alors installés avec l'application.

Voici ici comment installé un paquet sans ces paquets recommandés qui parfois ne sont pas utiles et prennent de la place sur le disque dur.

Il faut rajouter l'argument **--no-install-recommends** à apt.

`apt-get install  --no-install-recommends application`

Exemple avec mail-notification
------------------------------

Pour comprendre l'utilité de cette astuce, rien ne vaut un exemple avec l'installation d'une application tel que mail-notification.

     antoine@antoine:~$ sudo apt-get install mail-notification 
     Lecture des listes de paquets... 
     Fait Construction de l'arbre des dépendances        
     Lecture des informations d'état... 
     Fait Les paquets supplémentaires suivants seront installés :    bogofilter bogofilter-bdb bogofilter-common curl evolution evolution-common evolution-plugins evolution-webcal fetchyahoo   getlive libconvert-binhex-perl libcrypt-ssleay-perl libcurl3 libexchange-storage1.2-3 libgmime-2.0-2a libgnome-pilot2   libgtkhtml-editor-common libgtkhtml-editor0 libgtkhtml3.14-19 libio-socket-ssl-perl libio-stringy-perl liblpint-bonobo0   libmime-tools-perl libnet-libidn-perl libnet-ssleay-perl libpisock9 libpisync1 libpst4 mail-notification-evolution postfix   procmail ssl-cert 
     Paquets suggérés :   pax db4.7-util bug-buddy network-manager evolution-exchange evolution-dbg evolution-plugins-experimental gnome-pilot-conduits   libgtkhtml3.14-dbg libio-socket-inet6-perl jpilot pilot-link kpilot gnome-pilot claws-mail sylpheed postfix-mysql postfix-pgsql   postfix-ldap postfix-pcre sasl2-bin resolvconf postfix-cdb 
     Les NOUVEAUX paquets suivants seront installés :   bogofilter bogofilter-bdb bogofilter-common curl evolution evolution-common evolution-plugins evolution-webcal fetchyahoo   getlive libconvert-binhex-perl libcrypt-ssleay-perl libcurl3 libexchange-storage1.2-3 libgmime-2.0-2a libgnome-pilot2   libgtkhtml-editor-common libgtkhtml-editor0 libgtkhtml3.14-19 libio-socket-ssl-perl libio-stringy-perl liblpint-bonobo0   libmime-tools-perl libnet-libidn-perl libnet-ssleay-perl libpisock9 libpisync1 libpst4 mail-notification   mail-notification-evolution postfix procmail ssl-cert 
     0 mis à jour, 33 nouvellement installés, 0 à enlever et 0 non mis à jour. Il est nécessaire de prendre 10,8Mo dans les archives. Après cette opération, 85,4Mo d'espace disque supplémentaires seront utilisés. Souhaitez-vous continuer O/n ? n

On peut remarquer ici que l'installation de mail-notification a engendré l'installation de ses dépendances puis l'installation de ses paquets recommandés et des dépendances de ces paquets recommandés.

Au final on se retrouve avec une installation qui demande un espace disque de **85,4Mo** .

On refait ici la même installation en annulant l'installation des paquets recommandés.

     antoine@antoine:~$ sudo apt-get install  --no-install-recommends mail-notification 
     Lecture des listes de paquets... 
     Fait Construction de l'arbre des dépendances        
     Lecture des informations d'état... 
     Fait Les paquets supplémentaires suivants seront installés :    libgmime-2.0-2a 
     Paquets recommandés :   mail-notification-evolution getlive fetchyahoo 
     Les NOUVEAUX paquets suivants seront installés :   libgmime-2.0-2a mail-notification 
     0 mis à jour, 2 nouvellement installés, 0 à enlever et 0 non mis à jour. Il est nécessaire de prendre 605ko dans les archives. Après cette opération, 2 040ko d'espace disque supplémentaires seront utilisés.

**2 040ko** !! la différence est énorme, on voit ici que les dépendances ont provoqué l'installation d'un seul paquet supplémentaire.

Avec cette astuce, on est passé de 85,4Mo à 2 040ko pour l'installation d'une application, ce qui n'est pas négligeable.

</p>

