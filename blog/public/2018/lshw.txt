antoine@Ninja:~$ sudo lshw -short
[sudo] Mot de passe de antoine : 
Chemin matériel  Périphérique  Classe      Description
=========================================================
                                  system      ESPRIMO P3520
/0                                bus         D2841-A1
/0/0                              memory      99KiB BIOS
/0/4                              processor   Intel(R) Core(TM)2 Duo CPU     E8400  @ 3.00GHz
/0/4/5                            memory      64KiB L1 cache
/0/4/6                            memory      6MiB L2 cache
/0/24                             memory      4GiB Mémoire Système
/0/24/0                           memory      2GiB DIMM DDR2 Synchrone 800 MHz (1,2 ns)
/0/24/1                           memory      2GiB DIMM DDR2 Synchrone 800 MHz (1,2 ns)
/0/100                            bridge      4 Series Chipset DRAM Controller
/0/100/1                          bridge      4 Series Chipset PCI Express Root Port
/0/100/1/0                        display     G98 [GeForce 9300 GE]
/0/100/1b                         multimedia  NM10/ICH7 Family High Definition Audio Controller
/0/100/1c                         bridge      NM10/ICH7 Family PCI Express Port 1
/0/100/1c.3                       bridge      NM10/ICH7 Family PCI Express Port 4
/0/100/1c.3/0     enp3s0          network     NetLink BCM5784M Gigabit Ethernet PCIe
/0/100/1d                         bus         NM10/ICH7 Family USB UHCI Controller #1
/0/100/1d/1       usb2            bus         UHCI Host Controller
/0/100/1d.1                       bus         NM10/ICH7 Family USB UHCI Controller #2
/0/100/1d.1/1     usb3            bus         UHCI Host Controller
/0/100/1d.2                       bus         NM10/ICH7 Family USB UHCI Controller #3
/0/100/1d.2/1     usb4            bus         UHCI Host Controller
/0/100/1d.3                       bus         NM10/ICH7 Family USB UHCI Controller #4
/0/100/1d.3/1     usb5            bus         UHCI Host Controller
/0/100/1d.7                       bus         NM10/ICH7 Family USB2 EHCI Controller
/0/100/1d.7/1     usb1            bus         EHCI Host Controller
/0/100/1e                         bridge      82801 PCI Bridge
/0/100/1f                         bridge      82801GB/GR (ICH7 Family) LPC Interface Bridge
/0/100/1f.2                       storage     NM10/ICH7 Family SATA Controller [IDE mode]
/0/100/1f.3                       bus         NM10/ICH7 Family SMBus Controller
/0/1              scsi0           storage     
/0/1/0.0.0        /dev/sda        disk        500GB ST3500830AS
/0/1/0.0.0/1      /dev/sda1       volume      19GiB Volume EXT4
/0/1/0.0.0/2      /dev/sda2       volume      19GiB Volume EXT4
/0/1/0.0.0/3      /dev/sda3       volume      1999MiB Linux swap volume
/0/1/0.0.0/4      /dev/sda4       volume      424GiB Volume EXT4
/0/2              scsi1           storage     
/0/2/0.0.0        /dev/cdrom      disk        DVD RW AD-7230S
/1                                power       S26113-E547-V50
antoine@Ninja:~$ sudo lshw
ninja                       
    description: Ordinateur Mini-Tour
    produit: ESPRIMO P3520
    fabriquant: FUJITSU SIEMENS
    numéro de série: YKRG028058
    bits: 64 bits
    fonctionnalités: smbios-2.5 dmi-2.5 smp vsyscall32
    configuration: boot=normal chassis=mini-tower uuid=93CD554C-5BE7-1E79-A7EC-001999735A41
  *-core
       description: Carte mère
       produit: D2841-A1
       fabriquant: FUJITSU SIEMENS
       identifiant matériel: 0
       version: S26361-D2841-A1
       numéro de série: 31567943
     *-firmware
          description: BIOS
          fabriquant: FUJITSU SIEMENS // Phoenix Technologies Ltd.
          identifiant matériel: 0
          version: 6.00 R1.04.2841.A1
          date: 07/31/2009
          taille: 99KiB
          capacité: 960KiB
          fonctionnalités: pci pnp upgrade shadowing escd cdboot bootselect int13floppy360 int13floppy1200 int13floppy720 int13floppy2880 int5printscreen int9keyboard int14serial int17printer int10video acpi usb ls120boot zipboot biosbootspecification
     *-cpu
          description: CPU
          produit: Intel(R) Core(TM)2 Duo CPU     E8400  @ 3.00GHz
          fabriquant: Intel Corp.
          identifiant matériel: 4
          information bus: cpu@0
          version: Intel(R) Core(TM)2 Duo CPU E8400
          emplacement: CPU
          taille: 2018MHz
          capacité: 3GHz
          bits: 64 bits
          fonctionnalités: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx constant_tsc arch_perfmon pebs bts rep_good nopl cpuid aperfmperf pni dtes64 monitor ds_cpl vmx smx est tm2 ssse3 cx16 xtpr pdcm sse4_1 xsave lahf_lm pti tpr_shadow vnmi flexpriority dtherm cpufreq
          configuration: cores=2 enabledcores=2 threads=2
        *-cache:0
             description: L1 cache
             identifiant matériel: 5
             emplacement: L1 Cache
             taille: 64KiB
             capacité: 64KiB
             fonctionnalités: burst synchronous internal write-through data
             configuration: level=1
        *-cache:1
             description: L2 cache
             identifiant matériel: 6
             emplacement: L2 Cache
             taille: 6MiB
             capacité: 8MiB
             fonctionnalités: burst internal write-back unified
             configuration: level=2
     *-memory
          description: Mémoire Système
          identifiant matériel: 24
          emplacement: Carte mère
          taille: 4GiB
        *-bank:0
             description: DIMM DDR2 Synchrone 800 MHz (1,2 ns)
             produit: 4D3320373854353636334548332D43463720
             fabriquant: Samsung
             identifiant matériel: 0
             numéro de série: 80C961FD
             emplacement: Slot-1
             taille: 2GiB
             bits: 64 bits
             horloge: 800MHz (1.2ns)
        *-bank:1
             description: DIMM DDR2 Synchrone 800 MHz (1,2 ns)
             produit: 4D3320373854353636334548332D43463720
             fabriquant: Samsung
             identifiant matériel: 1
             numéro de série: 80C961E3
             emplacement: Slot-2
             taille: 2GiB
             bits: 64 bits
             horloge: 800MHz (1.2ns)
     *-pci
          description: Host bridge
          produit: 4 Series Chipset DRAM Controller
          fabriquant: Intel Corporation
          identifiant matériel: 100
          information bus: pci@0000:00:00.0
          version: 03
          bits: 32 bits
          horloge: 33MHz
        *-pci:0
             description: PCI bridge
             produit: 4 Series Chipset PCI Express Root Port
             fabriquant: Intel Corporation
             identifiant matériel: 1
             information bus: pci@0000:00:01.0
             version: 03
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: pci pm msi pciexpress normal_decode bus_master cap_list
             configuration: driver=pcieport
             ressources: irq:16 portE/S:2000(taille=4096) mémoire:dd000000-dfffffff portE/S:e0000000(taille=268435456)
           *-display
                description: VGA compatible controller
                produit: G98 [GeForce 9300 GE]
                fabriquant: NVIDIA Corporation
                identifiant matériel: 0
                information bus: pci@0000:01:00.0
                version: a1
                bits: 64 bits
                horloge: 33MHz
                fonctionnalités: pm msi pciexpress vga_controller bus_master cap_list rom
                configuration: driver=nouveau latency=0
                ressources: irq:24 mémoire:dd000000-ddffffff mémoire:e0000000-efffffff mémoire:de000000-dfffffff portE/S:2000(taille=128) mémoire:c0000-dffff
        *-multimedia
             description: Audio device
             produit: NM10/ICH7 Family High Definition Audio Controller
             fabriquant: Intel Corporation
             identifiant matériel: 1b
             information bus: pci@0000:00:1b.0
             version: 01
             bits: 64 bits
             horloge: 33MHz
             fonctionnalités: pm msi pciexpress bus_master cap_list
             configuration: driver=snd_hda_intel latency=0
             ressources: irq:25 mémoire:fc300000-fc303fff
        *-pci:1
             description: PCI bridge
             produit: NM10/ICH7 Family PCI Express Port 1
             fabriquant: Intel Corporation
             identifiant matériel: 1c
             information bus: pci@0000:00:1c.0
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: pci pciexpress msi pm normal_decode bus_master cap_list
             configuration: driver=pcieport
             ressources: irq:18 portE/S:3000(taille=4096) mémoire:c0000000-c01fffff portE/S:c0200000(taille=2097152)
        *-pci:2
             description: PCI bridge
             produit: NM10/ICH7 Family PCI Express Port 4
             fabriquant: Intel Corporation
             identifiant matériel: 1c.3
             information bus: pci@0000:00:1c.3
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: pci pciexpress msi pm normal_decode bus_master cap_list
             configuration: driver=pcieport
             ressources: irq:20 portE/S:4000(taille=4096) mémoire:fc200000-fc2fffff portE/S:c0400000(taille=2097152)
           *-network
                description: Ethernet interface
                produit: NetLink BCM5784M Gigabit Ethernet PCIe
                fabriquant: Broadcom Limited
                identifiant matériel: 0
                information bus: pci@0000:03:00.0
                nom logique: enp3s0
                version: 10
                numéro de série: 00:19:99:73:5a:41
                taille: 1Gbit/s
                capacité: 1Gbit/s
                bits: 64 bits
                horloge: 33MHz
                fonctionnalités: pm vpd msi pciexpress bus_master cap_list ethernet physical tp 10bt 10bt-fd 100bt 100bt-fd 1000bt 1000bt-fd autonegotiation
                configuration: autonegotiation=on broadcast=yes driver=tg3 driverversion=3.137 duplex=full firmware=sb v2.19 ip=192.168.1.13 latency=0 link=yes multicast=yes port=twisted pair speed=1Gbit/s
                ressources: irq:26 mémoire:fc200000-fc20ffff
        *-usb:0
             description: USB controller
             produit: NM10/ICH7 Family USB UHCI Controller #1
             fabriquant: Intel Corporation
             identifiant matériel: 1d
             information bus: pci@0000:00:1d.0
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: uhci bus_master
             configuration: driver=uhci_hcd latency=0
             ressources: irq:20 portE/S:1820(taille=32)
           *-usbhost
                produit: UHCI Host Controller
                fabriquant: Linux 4.15.0-34-generic uhci_hcd
                identifiant matériel: 1
                information bus: usb@2
                nom logique: usb2
                version: 4.15
                fonctionnalités: usb-1.10
                configuration: driver=hub slots=2 speed=12Mbit/s
        *-usb:1
             description: USB controller
             produit: NM10/ICH7 Family USB UHCI Controller #2
             fabriquant: Intel Corporation
             identifiant matériel: 1d.1
             information bus: pci@0000:00:1d.1
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: uhci bus_master
             configuration: driver=uhci_hcd latency=0
             ressources: irq:22 portE/S:1840(taille=32)
           *-usbhost
                produit: UHCI Host Controller
                fabriquant: Linux 4.15.0-34-generic uhci_hcd
                identifiant matériel: 1
                information bus: usb@3
                nom logique: usb3
                version: 4.15
                fonctionnalités: usb-1.10
                configuration: driver=hub slots=2 speed=12Mbit/s
        *-usb:2
             description: USB controller
             produit: NM10/ICH7 Family USB UHCI Controller #3
             fabriquant: Intel Corporation
             identifiant matériel: 1d.2
             information bus: pci@0000:00:1d.2
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: uhci bus_master
             configuration: driver=uhci_hcd latency=0
             ressources: irq:21 portE/S:1860(taille=32)
           *-usbhost
                produit: UHCI Host Controller
                fabriquant: Linux 4.15.0-34-generic uhci_hcd
                identifiant matériel: 1
                information bus: usb@4
                nom logique: usb4
                version: 4.15
                fonctionnalités: usb-1.10
                configuration: driver=hub slots=2 speed=12Mbit/s
        *-usb:3
             description: USB controller
             produit: NM10/ICH7 Family USB UHCI Controller #4
             fabriquant: Intel Corporation
             identifiant matériel: 1d.3
             information bus: pci@0000:00:1d.3
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: uhci bus_master
             configuration: driver=uhci_hcd latency=0
             ressources: irq:17 portE/S:1880(taille=32)
           *-usbhost
                produit: UHCI Host Controller
                fabriquant: Linux 4.15.0-34-generic uhci_hcd
                identifiant matériel: 1
                information bus: usb@5
                nom logique: usb5
                version: 4.15
                fonctionnalités: usb-1.10
                configuration: driver=hub slots=2 speed=12Mbit/s
        *-usb:4
             description: USB controller
             produit: NM10/ICH7 Family USB2 EHCI Controller
             fabriquant: Intel Corporation
             identifiant matériel: 1d.7
             information bus: pci@0000:00:1d.7
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: pm debug ehci bus_master cap_list
             configuration: driver=ehci-pci latency=0
             ressources: irq:20 mémoire:fc304000-fc3043ff
           *-usbhost
                produit: EHCI Host Controller
                fabriquant: Linux 4.15.0-34-generic ehci_hcd
                identifiant matériel: 1
                information bus: usb@1
                nom logique: usb1
                version: 4.15
                fonctionnalités: usb-2.00
                configuration: driver=hub slots=8 speed=480Mbit/s
        *-pci:3
             description: PCI bridge
             produit: 82801 PCI Bridge
             fabriquant: Intel Corporation
             identifiant matériel: 1e
             information bus: pci@0000:00:1e.0
             version: e1
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: pci subtractive_decode bus_master cap_list
        *-isa
             description: ISA bridge
             produit: 82801GB/GR (ICH7 Family) LPC Interface Bridge
             fabriquant: Intel Corporation
             identifiant matériel: 1f
             information bus: pci@0000:00:1f.0
             version: 01
             bits: 32 bits
             horloge: 33MHz
             fonctionnalités: isa bus_master cap_list
             configuration: driver=lpc_ich latency=0
             ressources: irq:0
        *-ide
             description: IDE interface
             produit: NM10/ICH7 Family SATA Controller [IDE mode]
             fabriquant: Intel Corporation
             identifiant matériel: 1f.2
             information bus: pci@0000:00:1f.2
             version: 01
             bits: 32 bits
             horloge: 66MHz
             fonctionnalités: ide pm bus_master cap_list
             configuration: driver=ata_piix latency=0
             ressources: irq:19 portE/S:1f0(taille=8) portE/S:3f6 portE/S:170(taille=8) portE/S:376 portE/S:18c0(taille=16) mémoire:c0600000-c06003ff
        *-serial NON-RÉCLAMÉ
             description: SMBus
             produit: NM10/ICH7 Family SMBus Controller
             fabriquant: Intel Corporation
             identifiant matériel: 1f.3
             information bus: pci@0000:00:1f.3
             version: 01
             bits: 32 bits
             horloge: 33MHz
             configuration: latency=0
             ressources: portE/S:1100(taille=32)
     *-scsi:0
          identifiant matériel: 1
          nom logique: scsi0
          fonctionnalités: emulated
        *-disk
             description: ATA Disk
             produit: ST3500830AS
             fabriquant: Seagate
             identifiant matériel: 0.0.0
             information bus: scsi@0:0.0.0
             nom logique: /dev/sda
             version: D
             numéro de série: 6QG3LXC0
             taille: 465GiB (500GB)
             fonctionnalités: gpt-1.00 partitioned partitioned:gpt
             configuration: ansiversion=5 guid=2e08e3c1-2dce-4e14-83f5-97d028546acb logicalsectorsize=512 sectorsize=512
           *-volume:0
                description: Volume EXT4
                fabriquant: Linux
                identifiant matériel: 1
                information bus: scsi@0:0.0.0,1
                nom logique: /dev/sda1
                nom logique: /
                version: 1.0
                numéro de série: d14744eb-2d82-4453-af31-27c30e236d15
                taille: 19GiB
                fonctionnalités: journaled extended_attributes large_files huge_files dir_nlink 64bit extents ext4 ext2 initialized
                configuration: created=2018-09-22 23:30:46 filesystem=ext4 lastmountpoint=/ modified=2018-09-23 13:12:23 mount.fstype=ext4 mount.options=rw,relatime,errors=remount-ro,data=ordered mounted=2018-09-23 00:31:00 name=/ state=mounted
           *-volume:1
                description: Volume EXT4
                fabriquant: Linux
                identifiant matériel: 2
                information bus: scsi@0:0.0.0,2
                nom logique: /dev/sda2
                nom logique: /home
                version: 1.0
                numéro de série: 886c0bbd-6398-43fe-a03f-45c9464c7de0
                taille: 19GiB
                fonctionnalités: journaled extended_attributes large_files huge_files dir_nlink recover 64bit extents ext4 ext2 initialized
                configuration: created=2018-09-22 23:30:49 filesystem=ext4 lastmountpoint=/home modified=2018-09-23 13:12:40 mount.fstype=ext4 mount.options=rw,relatime,data=ordered mounted=2018-09-23 13:12:40 name=/home state=mounted
           *-volume:2
                description: Linux swap volume
                fabriquant: Linux
                identifiant matériel: 3
                information bus: scsi@0:0.0.0,3
                nom logique: /dev/sda3
                version: 1
                numéro de série: 514e099a-5e7f-4df2-880d-0d4b9482f03a
                taille: 1999MiB
                capacité: 1999MiB
                fonctionnalités: nofs swap initialized
                configuration: filesystem=swap pagesize=4095
           *-volume:3
                description: Volume EXT4
                fabriquant: Linux
                identifiant matériel: 4
                information bus: scsi@0:0.0.0,4
                nom logique: /dev/sda4
                nom logique: /document
                version: 1.0
                numéro de série: 8e2d0d4f-9782-451f-a254-82a52e09f953
                taille: 424GiB
                fonctionnalités: journaled extended_attributes large_files huge_files dir_nlink recover 64bit extents ext4 ext2 initialized
                configuration: created=2018-09-22 23:30:52 filesystem=ext4 modified=2018-09-23 13:12:40 mount.fstype=ext4 mount.options=rw,relatime,data=ordered mounted=2018-09-23 13:12:40 name=/document state=mounted
     *-scsi:1
          identifiant matériel: 2
          nom logique: scsi1
          fonctionnalités: emulated
        *-cdrom
             description: DVD-RAM writer
             produit: DVD RW AD-7230S
             fabriquant: Optiarc
             identifiant matériel: 0.0.0
             information bus: scsi@1:0.0.0
             nom logique: /dev/cdrom
             nom logique: /dev/cdrw
             nom logique: /dev/dvd
             nom logique: /dev/dvdrw
             nom logique: /dev/sr0
             version: 1.83
             fonctionnalités: removable audio cd-r cd-rw dvd dvd-r dvd-ram
             configuration: ansiversion=5 status=nodisc
  *-power NON-RÉCLAMÉ
       description: S26113-E547-V50
       identifiant matériel: 1
       version: GS01 REV04
       numéro de série: 135301
       capacité: 300mWh
antoine@Ninja:~$ lspci
00:00.0 Host bridge: Intel Corporation 4 Series Chipset DRAM Controller (rev 03)
00:01.0 PCI bridge: Intel Corporation 4 Series Chipset PCI Express Root Port (rev 03)
00:1b.0 Audio device: Intel Corporation NM10/ICH7 Family High Definition Audio Controller (rev 01)
00:1c.0 PCI bridge: Intel Corporation NM10/ICH7 Family PCI Express Port 1 (rev 01)
00:1c.3 PCI bridge: Intel Corporation NM10/ICH7 Family PCI Express Port 4 (rev 01)
00:1d.0 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #1 (rev 01)
00:1d.1 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #2 (rev 01)
00:1d.2 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #3 (rev 01)
00:1d.3 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #4 (rev 01)
00:1d.7 USB controller: Intel Corporation NM10/ICH7 Family USB2 EHCI Controller (rev 01)
00:1e.0 PCI bridge: Intel Corporation 82801 PCI Bridge (rev e1)
00:1f.0 ISA bridge: Intel Corporation 82801GB/GR (ICH7 Family) LPC Interface Bridge (rev 01)
00:1f.2 IDE interface: Intel Corporation NM10/ICH7 Family SATA Controller [IDE mode] (rev 01)
00:1f.3 SMBus: Intel Corporation NM10/ICH7 Family SMBus Controller (rev 01)
01:00.0 VGA compatible controller: NVIDIA Corporation G98 [GeForce 9300 GE] (rev a1)
03:00.0 Ethernet controller: Broadcom Limited NetLink BCM5784M Gigabit Ethernet PCIe (rev 10)
