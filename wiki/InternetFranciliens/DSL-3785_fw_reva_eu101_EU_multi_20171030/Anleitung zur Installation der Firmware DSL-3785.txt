Anleitung zur Installation der Firmware
F�r DSL-3785 Rev.A

Zur Installation der Firmware gehen Sie bitte wie folgt vor.


1. Greifen Sie per Webbrowser auf die Konfiguration Ihres DSL-3785 zu.
Die Standard Adresse ist http://192.168.1.1


2. Standardm��ig ist auf der Konfiguration das Kennwort admin gesetzt.

W�hlen Sie oben-rechts im Aufklappmen� Deutsch aus, wenn die Konfigurationsoberfl�che noch nicht in Deutsch angezeigt wird, geben als Kennwort admin ein

Wird das CAPTCH-Feld angezeigt geben Sie das angezeigte CAPTCHA ein.

Klicken auf Anmelden.

Hinweis:
Haben Sie bereits ein Admin-Kennwort konfiguriert, geben Sie dieses an.
Kennen Sie das vergebene Admin-Kennwort nicht mehr, m�ssen Sie den DSL-3785 auf Werkseinstellungen zur�cksetzen (Factory Reset).

Halten Sie dazu den Resettaster mit der Aufschrift RESET auf der R�ckseite des eingeschalteten DSL-3785 mit z.B. einer B�roklammer oder einer Nadel f�r 5-10 Sekunden gedr�ckt.
Die LEDs Ihres DSL-3785 leuchten gr�n auf, lassen Sie dann den Resettaster los.


3. Wurde Ihr DSL-3785 bereits zuvor konfiguriert oder der Setup-Assistent zuvor schon mal abgebrochen, erscheint der Setup-Assistent.

Klicken oben-rechts auf X.


4. W�hlen Sie oben das Men� Verwaltung und dann im sich �ffnenden Men� Firmware-Upgrade aus.


5. Unten klicken Sie unten bei Manuelles Upgrade auf den Knopf Datei w�hlen.

W�hlen Sie die zuvor entpackte Firmwaredatei "DSL-3785_Firmware_RevA_EU101_10312017.bin" aus und klicken auf Hochladen.


6. Klicken Sie auf OK.


7. Die Firmware wird nun installiert.
Dies dauert etwa 4 Minuten.


8. Klicken Sie auf OK.


Die Installation der Firmware ist damit abgeschlossen.




Empfehlung: Factory Reset

Es kann vorkommen, dass sich Bugs (Fehlfunktionen) mit der bestehenden Konfiguration von der alten Firmware in die neue Firmware einschleppen. Oder Bugs auftreten, welche die neue Firmware gar nicht hat, da die alte Konfiguration in manchen Punkten mit der neuen Firmware nicht zusammenpasst.
Daher empfehlen wir abschlie�end einen Factory Reset mit anschlie�ender Neueinrichtung des Routers.


M�glichkeit 1: �ber den Reset-Knopf Ihres DSL-3785:

Halten Sie dazu den Resettaster mit der Aufschrift RESET auf der R�ckseite des eingeschalteten DSL-3785 mit z.B. einer B�roklammer oder einer Nadel f�r 5-10 Sekunden gedr�ckt.
Die LEDs Ihres DSL-3785 leuchten gr�n auf, lassen Sie dann den Resettaster los.

Installieren Sie bitte keine Konfigurationsdatei einer anderen, �lteren Firmwareversion in Ihren DSL-3785.



M�glichkeit 2: �ber die Konfigurationsoberfl�che Ihres DSL-3785:

W�hlen Sie oben das Men� Verwaltung und dann im sich �ffnenden Men� Systemeinstellungen aus.

Klicken Sie auf Ger�t wiederherstellen.

Klicken Sie auf OK.

Ihr DSL-3785 startet nun neu.
Dies dauert 90 Sekunden.

Der Factory Reset ist damit abgeschlossen.

Installieren Sie bitte keine Konfigurationsdatei einer anderen, �lteren Firmwareversion in Ihren DSL-3785.





Um die genaue Versionsnummer der im DSL-3785 installierten Firmware auszulesen, loggen Sie sich in die Konfiguration Ihres DSL-3785 ein.

W�hlen Sie oben das Men� Verwaltung und dann im sich �ffnenden Men� Firmware-Upgrade aus.

Bei Firmware-Info ist anhand der Versionsnummer und des Datums zu erkennen, welche Firmwareversion in Ihrem DSL-3785 installiert ist.
