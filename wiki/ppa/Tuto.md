# Création .deb sur ppa

## Pré-requis
 * Avoir un compte sur launchpad
 * Y avoir créer un dépot ppa
 * le fichier setup.py est bien configuré (logiciel en python3)

# Création du dossier debian

Le dossier debian est un dossier qui contient plusieurs fichiers de configuations utiles à la création du .deb.  

On va dans un premier temps récupérer les sources du logiciel, l'extraire pour y créer ce dossier debian.

    wget https://framagit.org/anto1ne/studmanager/-/archive/v0.2.3/studmanager-v0.2.3.tar.gz
    tar xvzf studmanager-v0.2.3.tar.gz

Renommer le fichier studmanager-v0.2.3.tar.gz en studmanager**_**v0.2.3**.orig**.tar.gz.  

Ensuite ouvrir le terminal dans le dossier studmanager-v0.2.3 et y créer le dossier debian.

    dh_make -e votreaddresse@email.tld -s -y

Il faut ensuite remplir plusieurs fichiers, les plus importants sont : 

1. Le fichier **control** : il faut faire attention aux choix des paquets pour builds-depends (pour compiler) et à ceux pour faire tourner le logiciel (build). Le maintainer doit être décrit de manière à être reconnu par gpg ('gpg -k')

```
    Source: studmanager  
    Section: misc  
    Priority: optional  
    Maintainer: Antoine (2019) <antoine-@laposte.net>  
    Build-Depends: debhelper (>= 10), python3-dev (>=3.6.7), python3-setuptools (>=39.0.1), python3-pyqt5 (>=5.10.1), python3 (>=3.6.7)  
    Standards-Version: 4.1.2  
    Homepage: https://framagit.org/anto1ne/studmanager  
    #Vcs-Git: https://anonscm.debian.org/git/collab-maint/studmanager.git  
    #Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/studmanager.git  
      
    Package: studmanager  
    Architecture: amd64  
    Depends: ${misc:Depends}, ${python3:Depends}, python3-pyqt5 (>=5.10.1), python3 (>=3.6.7)  
    Description: This software helps you to take care of your horse.  
     This software is a tool designed to help you to take care of your horses.  
     It has the following features :  
      * Health Records  
      * Movement Records  
      * Address Book  
      * Reminders  
      * Cross platform software  
```

2. Le fichier **changelog**  : la date est celle retournée par la commande 'date -R'

```
    studmanager (0.2.3-0ubuntu2) bionic; urgency=low
    
      * Debian package is now available
        Fix #43 - About window does not open
    
     -- Antoine (2019) <antoine-@laposte.net>  Sat, 15 Feb 2020 16:18:18 +0100
    
    studmanager (0.2.1-0ubuntu1) bionic; urgency=low
    
      * Improve UI ergonomy
        Improve translation support
        Solves many bugs
        New Dashboard
        Add several horse breed
    
     -- Antoine (2019) <antoine-@laposte.net>  Sun, 09 Feb 2020 12:59:53 +0100
```    

3. Le fichier **copyright** pour mentionner la licence, ce fichier doit suivre un format bien particulier.

```
    Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/  
    Upstream-Name: studmanager  
    Upstream-Contact: Antoine (2019) <antoine-@laposte.net>  
    Source: https://framagit.org/anto1ne/studmanager  
    
    Files: *  
    Copyright: Antoine (2019) <antoine-@laposte.net>  
    License: GPL-3+  
     On Debian systems, the full text of the GNU General Public  
     License version 3 can be found in the file  
     `/usr/share/common-licenses/GPL-3'.  
```    

4. le fichier **rules** (le plus complexe) : définit les actions à réaliser pour la création du .deb

```
    #!/usr/bin/make -f  
    %:  
    	dh $@ --with python3  
    
    override_dh_auto_build:  
    	python3 setup.py build  
    
    override_dh_auto_install:  
    	python3 ./setup.py install --root=$(CURDIR)/debian/tmp --install-layout=deb  
    
    override_dh_auto_test:  
    	#python3 -m unittest discover -vv  
    	echo "Test unitaire"  
    
    override_dh_auto_clean:  
    	python3 setup.py clean -a  
    	rm -rf build  
    	rm -rf *.egg-info  
    	find . -name __pycache__ | xargs rm -rf  
```

5. Le fichier **install** : définit où doivent être stockés les différents fichiers 

```
    src/*                                     usr/share/studmanager
    studmanager                               usr/bin
    StudManager.desktop                       usr/share/applications
```    
6. Le fichier logiciel.desktop est le raccourci que l'on retrouvera dans le menu 

```
    [Desktop Entry]  
    Name=StudManager  
    Version=1.0  
    Exec=studmanager  
    Comment=Take care of your horses  
    Icon=/usr/share/studmanager/icons/horse.svg  
    Type=Application  
    Terminal=false  
    StartupNotify=true  
    Encoding=UTF-8  
    Categories=Office;  
```

## Premiers tests

Lancer successivement 
```
    debuild -S -sa # le mot de passe de votre clef pgp peut vous être demandé  
    debuild -us -uc  
```
Cela va créer un .deb que l'on peut tester localement avant un envoi sur launchpad. Être attentif aux messages d'erreurs pour corriger les différents fichiers.

## Envoi sur le ppa

```
    debuild -S -sa  
    dput ppa:antoine-/studmanager studmanager_0.2.3-0ubuntu2_source.changes  
```

Le .deb devrait être généré dans les minutes qui suivent.

## Sources

[Doc Ubuntu 1](https://doc.ubuntu-fr.org/tutoriel/creer_et_administrer_un_ppa_sur_launchpad)  
[Doc Ubuntu 2](https://doc.ubuntu-fr.org/tutoriel/creer_un_paquet)

