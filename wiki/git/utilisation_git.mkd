# Git

Git est un outil utilisé pour développer du code de manière collaborative.

## Utilisation courante

Après avoir créer un projet sur github, il suffit de récupérer son adresse pour le cloner localement.

    git clone git@github.com:toitoinebzh/apps.git

Après avoir réaliser des modifications du code, il est utile de taper la commande suivant pour avoir un aperçu des fichiers qui ont été modifiés.

    git status

Il faut par la suite ajouter les fichiers modifiés que l'on souhaite officiellement prendre en compte dans l'évolution du code

    git add monfichier

Il est ensuite nécessaire de fournir un commentaire expliquant la raison de la modification.

    git commit -m "ajout de monfichier"

Enfin, la modification peut être envoyé sur le dépot (sur le compte perso) pour être accessible par les collaborateurs.

    git push origin master

Si un collaborateur a entre temps réaliser des modifications, il faut mettre à jour ces fichiers locaux par l'intermédiare de 

    git pull origin master

Pour éviter les conflits et clarifier les actions réalisées, la création de *branch* se révèle très pertinente. Pour lister les branch existantes, un simple

    git branch 

fera l'affaire. Pour créer une branch, il faut par exemple entrer

    git branch correction

Pour changer de branch de travail, la commande suivante est utile

    git checkout master


## Forker avec git 

*Exemple ici avec le dépot https://github.com/YunoHost/apps*

Sur Github, clicker sur "fork", cela crée un dépôt identique sur son compte perso. Pour travailler sur son dépôt en local, il faut le cloner.

    git clone git@github.com:toitoinebzh/apps.git


### Mise à jour du dépôt

Pour prendre en compte les mises à jour qui ont lieu sur le dépôt original, il faut indiquer le "remote".

    git remote add upstream https://github.com/YunoHost/apps

Puis faire une mise à jour sur son dépot local (sur le pc)

    git pull --rebase upstream master

Pour que les modifications se retrouvent également sur le dépot (compte perso Github), il faut taper la commande suivante

    git push origin master 

### Pull Request

Lors qu'une branch est créée et "pusher" sur le comtpe perso, Github propose automatiquement un lien pour créer la pull request.

## Créer une étiquette

Pour avoir un suivi des versions stables de son code, un outil utile est d'utiliser les étiquettes ou tag en anglais. L'ajout d'un tag se fait simplement par 

    git tag v0.1.1

Par défaut le tag est stocké en local et non remoté sur le dépot distant. Pour diffuser l'info, un push est nécessaire.

    git push origin v0.1.1 

## Configurer son terminal lors de la première utilisation

Il est nécessaire de régler son terminal afin de pouvoir profiter des fonctions de git.

    git config --global user.name "Yves Saboret" 
    git config --global user.email "mettre ici votre adresse mail"

Afin de créer un lien entre son pc local et le dépot distant, il est nécessaire de générer une clef ssh à partager sur le git du dépot distant.

    ssh-keygen
    cat .ssh/*.pub

## Liens utiles

https://www.atlassian.com/git/tutorials/syncing

https://docs.framasoft.org/fr/gitlab/3-installation-poste.html