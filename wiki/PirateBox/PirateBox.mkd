# Fabrication d'une PirateBox à partir d'un vieux smartphone

![PirateBox-logo](PirateBox-logo.svg)

Ce document présente la démarche suivant pour transformer un vieux smartphone en PirateBox.

Une PirateBox est un dispositif qui permet de mettre en place un réseau sans fil local dans lequel les personnes connectés peuvent échanger des fichiers ou tout simplement discuter en ligne.

Il est possible de mettre en place une PirateBox sur de nombreux moyens comme un rapsberry pi, un routeur ou un ordinateur. La suite de ce document présente une installation sur un smartphone passé de mode...un bon moyen de lutter contre l'obsolescence programmée.

## Matériel

Le smartphone utilisé est un Wiko Iggy qui date de septembre 2013. Ce genre de téléphone peut se trouver d'occasion pour une trentaine d'euros.

  * Wiko Iggy
  * 1.4 Go de stockage
  * 512 Mo de ram
  * Android 4.2.2
  * Ecran 4.5"


## Procédure d'installation

La procédure d'installation est relativement simple, le point le plus délicat restant de rooter le téléphone, voilà la procédure que j'ai suivie :

 1. Réinitialisation du téléphone.  
 2. Pas besoin de fournir le compte google au premier démarrage.
 3. Rooter Android à l'aide de [KingRoot](https://kingroot.net/tutorials). L'APK doit être télécharger directement sur le site web de KingRoot.
 4. Téléchargement et installation de l'application [PirateBox](https://play.google.com/store/apps/details?id=de.fun2code.android.piratebox), téléchargée sur le [lien suivant ](https://cloudapks.com/app/de.fun2code.android.piratebox/) (version 0.5.9 beta dans mon cas)
 5. La configuration du partage se fait en allant dans 
     * Paramètres > Partage de connexion > Hotspot Wifi
     * Laisser le hostspot allumé > toujours
     * Configurer le point d'accès Wifi > nom du SSID et nb d'utilisateurs max à configurer
 6. Possibilité de changer des options depuis l'application PirateBox (langue, taille max des fichiers, etc.)
 7. Arrêter et redémarrer l'application pour prendre en compte les préférences. Depuis le smartphone, possibilité d'accéder à l'interface PirateBox en tapant l'adresse http://192.168.43.1:8080
 8. (bonus) Possibilité d'installer FDroid puis Yalp ou Aurora pour pouvoir ajouter des applications et réaliser des mises à jour.


## Se connecter à la PirateBox depuis un autre smartphone/ordinateur

 1. Se connecter au réseau wifi partagé **PirateBox - Share Freely**
 2. Le navigateur devrait automatiquement se rendre sur la page [pirate.box](http://pirate.box/)


Bon partage :-)


## Liens utiles

[Wikipedia](https://fr.wikipedia.org/wiki/PirateBox)  
[PirateBox.cc](https://piratebox.cc/start)
